from datetime import datetime

class MenuManager:
	
	@staticmethod
	def show_menu(): 
		menu_string = """\t1 - List of drivers.
	2 - List of busy drivers.
	3 - List of free drivers.
	4 - Get driver by number car.
	5 - Create driver.
	6 - Delete driver.
	7 - Update driver's data.
	8 - Is driver active by the number of car.
	9 - Exit"""

		print(menu_string)

	@staticmethod
	def get_by_number_car():
		number_car = input('Enter the number_car: ')
		return number_car

	@staticmethod
	def show_drivers(drivers):
		if not drivers:
			print("Taxi park is empty!")
			return
		for driver in drivers:
			print("*****************************")
			MenuManager.show_the_driver(driver)
			print("*****************************")

	@staticmethod
	def show_the_driver(driver):
		if not driver:
			print('Driver not found or does not exists')
			return

		print('Driver Name: ', driver.name)
		print('Driver Number Car: ', driver.number_car)
		readiness = 'Free' if driver.readiness else 'Busy'
		print('Driver Readiness: ', readiness)
		if not readiness: 
			print('Driver Time Order: ', datetime.srtftime(driver.time_order))
			print('Driver Order During: ', datetime.strftime(driver.during_order))

	@staticmethod
	def creating_of_driver():
		context = dict()
		context['name'] = input("Enter the driver name: ")
		context['number_car'] = input("Enter the number of car: ")
		context['readiness'] = True
		return context

	@staticmethod
	def updating_driver():
		context = {}
		context['new_number_car'] = input("Enter the new number of car or enter the old: ")
		context['name'] = input("Enter the new driver name or enter the old: ")
		return context

