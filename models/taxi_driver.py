from datetime import datetime, timedelta

class TaxiDriver:
	def __init__(self, name: str, number_car: int,
		readiness: bool=False, time_order: datetime=None, during_order: timedelta=None): 
		self.name = name
		self.number_car = number_car
		self.readiness = readiness
		self.time_order = time_order
		self.during_order = timedelta

	def is_active(self): 
		return True if self.readiness else False
