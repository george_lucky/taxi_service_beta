from models.taxi_driver import TaxiDriver


class TaxiManager:
	taxi_park = dict()
	
	def create_driver(self, name: str, number_car: int, 
					  readiness: bool=False, time_order=None,
					  during_order=None):
		
		driver = TaxiDriver(name, number_car, readiness, time_order, during_order)
		
		self.taxi_park[number_car] = driver
		return driver

	
	def get_driver_by_number_car(self, number_car):
		try:
			driver = self.taxi_park[number_car]
			return driver
		except KeyError:
			return None

	
	def update_driver_data(self, number_car, name: str=None, readiness: bool=None, 
						   new_number_car:str=None):
		driver = self.taxi_park[number_car]
		self.taxi_park.pop(number_car)
		
		if name: 
			driver.name = name
		if readiness is True:
			driver.readiness = True
			driver.time_order = None
			driver.during_order = None
		if new_number_car:
			driver.number_car = new_number_car
			self.taxi_park[new_number_car] = driver
			return driver
		self.taxi_park[number_car] = driver
		return driver

	
	def delete_driver(self, number_car):
		deleted_driver = self.taxi_park.pop(number_car)
		return deleted_driver

	
	def get_all_drivers(self):
		drivers = [i for i in self.taxi_park.values()]
		return drivers


	def get_free_drivers(self):
		drivers = [i for i in self.taxi_park.values() if i.readiness] 
		return drivers


	def get_busy_drivers(self):
		drivers = [i for i in self.taxi_park.values() if not i.readiness]
		return drivers

	