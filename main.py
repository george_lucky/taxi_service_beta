from managers.taxi_manager import TaxiManager
from managers.menu_manager import MenuManager as MM
from constants import MenuTypes


manager = TaxiManager()

while True:
	MM.show_menu()
	choice_number = int(input())
	if choice_number == MenuTypes.LIST_OF_DRIVERS:
		drivers = manager.get_all_drivers()
		MM.show_drivers(drivers)

	if choice_number == MenuTypes.LIST_OF_BUSY_DRIVERS:
		drivers = manager.get_busy_drivers()
		MM.show_drivers(drivers)
	
	if choice_number == MenuTypes.LIST_OF_FREE_DRIVERS:
		drivers = manager.get_free_drivers()
		MM.show_drivers(drivers)	
	
	if choice_number == MenuTypes.DRIVER_BY_NUMBER_CAR:
		number_car = MM.get_by_number_car()
		driver = manager.get_driver_by_number_car(number_car)
		MM.show_the_driver(driver)
	
	if choice_number == MenuTypes.CREATE_DRIVER:
		dc = MM.creating_of_driver()
		manager.create_driver(dc['name'], dc['number_car'])
	
	if choice_number == MenuTypes.DELETE_DRIVER:
		number_car = MM.get_by_number_car()
		driver = manager.delete_driver(number_car)
	
	if choice_number == MenuTypes.UPDATE_DRIVER:
		number_car = MM.get_by_number_car(number_car)
		dc = MM.updating_driver()
		manager.updating_driver(number_car, name=dc['name'], new_number_car=dc['new_number_car'])
	
	if choice_number == MenuTypes.DRIVER_IS_ACTIVE:
		number_car = MM.get_by_number_car(number_car)
		driver = TaxiManager.get_by_number_car(number_car)
		print("Driver is free") if driver.readiness else print("Driver is busy")

	if choice_number == MenuTypes.EXIT_FROM_MENU:
		exit()